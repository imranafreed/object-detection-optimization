This file is trained on the custom dataset [images.zip](https://drive.google.com/file/d/1u5TLks_3ezO0ShCmEwuA8slX89JHwh7d/view?usp=sharing) provided by Imran.

The .weights files for [1000](https://drive.google.com/file/d/1qLrMFKFA_OaKaxB7SfTvlWvdB5gcCgdT/view?usp=sharing) and [2000](https://drive.google.com/file/d/1--ylZpyOJ09iCoFjVIwNyu3wIO1ZEtsP/view?usp=sharing) iterations are shared here.

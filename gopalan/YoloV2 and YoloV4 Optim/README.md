SuperviselyToDarknet contains code used to convert JSON annotations(Infolks Format) to YOLO-compatible txt files.

The other two files contain training code for the respective models, which is completely implemented using Darknet framework.

The files contain comments sufficient to understand the implementation. 'YOLOv4' has more detailed comments.













THE FOLLOWING HAS BEEN SCRAPPED:

>`Working on understanding and optimising YoloV2 Pytorch implementation given in [this repo](https://github.com/youzunzhi/YOLOv2-PyTorch), which is cloned here.`
>`Reference(YoloV2)-https://github.com/longcw/yolo2-pytorch`

>`Other references(YoloV3)-`

>`https://github.com/cv-core/MIT-Driverless-CV-TrainingInfra/blob/master/CVC-YOLOv3/models.py`

>`https://github.com/ultralytics/yolov3`\

>`https://github.com/eriklindernoren/PyTorch-YOLOv3`

>`References(YoloV4)-`

>`https://github.com/WongKinYiu/PyTorch_YOLOv4`

>`https://awesomeopensource.com/project/Tianxiaomo/pytorch-YOLOv4?categoryPage=6`

The colab notebook for training can be found [here](https://colab.research.google.com/drive/106xdT8Qxl13lRS2F485n5edP-kxQpMnX?usp=sharing).
The drive containing labels for the FSCOCO dataset in YOLO format can be found [here](https://drive.google.com/drive/folders/11xnKWWedGRmBiORc1xFAInalAijxwrcS?usp=sharing)

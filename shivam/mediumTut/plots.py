import numpy as np
import pandas as pd
import matplotlib.pyplot as plt 

filename=input("Enter filename : ")

df=pd.read_csv(filename)

x=df[df.columns[0]].to_numpy()
y=df[df.columns[1]].to_numpy()

plt.plot(x,y)
plt.show()


#include <bits/stdc++.h>
using namespace std ;

void add (int N, float *x, float *y){
	for (int i=0; i< N; i++){
		y[i]=y[i]+x[i];
	}

	
}

int main (){
	int N=1<<27;
	
	float *x=new float[N];
	float *y=new float[N];
	
	for (int i=0; i<N; i++){
		x[i]=1.0f;
		y[i]=2.0f;
		
	}
	
	double avg=0;
	
	clock_t t;
	
	for (int i=0; i<10; i++){
		t=clock();
		add(N,x,y);
		t=clock()-t;
		
		printf ("CPU Run - %d : time = %f ms \n", i, (((float)t)/CLOCKS_PER_SEC)*1000);
		avg+= (((float)t)/CLOCKS_PER_SEC)*1000;
	}
	
	printf ("Average time for 10 iterations = %lf ms \n", avg/10);
	
	delete[] x;
	delete[] y;
	return 0;
	
	
	
}

#include <bits/stdc++.h>
using namespace std;

double data[1025];

void csv(){
	char filename[11]="data1.csv";
	
	FILE *fp;
	fp=fopen(filename,"w+");
	fprintf(fp, "Number of Threads, Average Time");
	
	for (int i=1; i<=1024; i+=32){
		fprintf(fp, "\n%d,%lf",max(i,1),data[i]);
	}
		fclose(fp);
		
		printf("\n%s created", filename);
}

__global__
void add(int N, float *x, float *y){
	int t=threadIdx.x; //id of current thread
	int T=blockDim.x; // id of current block
	
	//blockDim = size of each dimension of current block 
	//gridDim = size of each dimension of current grid
	
	for (int i=t; i<N; i+=T){
		y[i]=y[i]+x[i];
	}
	
}

int main(){
	int N= 1<<27;
	float *x, *y;
	
	//allocates memory accessible to both gpu and cpu 
	
	cudaMallocManaged(&x, N*sizeof(float));
	cudaMallocManaged(&y, N*sizeof(float));
	
	for (int i=0; i<N; i++){
		x[i]=1.0f;
		y[i]=2.0f;
	}
	
	clock_t t;
	
	for (int i=1; i<=1024; i+=32){
		int T=i;
		
		double avg=0;
		
		for (int j=0; j<10; j++){
			t = clock();
			
			add<<<dim3(1,1,1), dim3(T,1,1)>>> (N,x,y);
			cudaDeviceSynchronize(); //wait for gpu to finish executing kernel

			t=clock()-t;
			
			printf("T = %d run = %d Time = %f\n" ,T, j, (((double)t/CLOCKS_PER_SEC)*1000));
			avg+=(((double)t/CLOCKS_PER_SEC)*1000);
		}
		avg=avg/10;
		data[i]=avg;
		
		printf ("Time take with %d threads is %lf\n", T, avg);
		
	}
	
	csv();
	cudaFree(x);
	cudaFree(y);
	
}
